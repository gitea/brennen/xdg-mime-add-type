#!/usr/bin/bash

export PATH="$PATH:./"

# This is pretty fucking stupid.

# https://stackoverflow.com/questions/2060284/how-to-use-the-xdg-mime-command
echo "Before adding type:"
xdg-mime query filetype test.zpl

echo
echo "Adding type:"
sudo xdg-mime install --mode system ./zebra-zpl.xml

echo
echo "After adding type:"
xdg-mime query filetype test.zpl

echo
echo "Adding desktop application:"

cat << DESKTOP | sudo tee /usr/share/applications/zpl-printer-script.desktop
[Desktop Entry]
Name=gVim
GenericName=ZPL Whatever
Comment=Do something to ZPL files
TryExec=zpl-printer-script
Exec=zpl-printer-script
Terminal=true
Type=Application
Keywords=Printer;ZPL;
Icon=gvim
Categories=Utility;
StartupNotify=true
MimeType=text/zpl
DESKTOP

echo
echo "Setting default application:"
sudo xdg-mime default zpl-printer-script.desktop text/zpl

echo
echo "Testing default application - should just cat the file:"
xdg-open ./test.zpl

echo
echo "If that worked, put zpl-printer-script in your path, add a line to"
echo "/etc/mime.types mapping text/zpl to the zpl extension, and try"
echo "with your browser."
